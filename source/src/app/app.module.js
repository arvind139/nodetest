(function() {
    'use strict';

    angular
        .module('app', [
            'ngResource',
            'lonceldemo',
            'ngSanitize', 'ngMaterial',
            'ui.router'
         ])
})();