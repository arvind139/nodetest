/**
 * Created by pankaj 17/07/2016
 */
(function ()
{
    'use strict';

    angular
        .module('lonceldemo')
        .factory('api', apiService);

    /** @ngInject */
    function apiService($resource)
    {

        var api = {};

        // Base Url
        api.baseUrl = '/';

        api.gitRepo = {
            repo: $resource('http://localhost:8000/fetchcommitdetails')
        };
        return api;
    }

})();
