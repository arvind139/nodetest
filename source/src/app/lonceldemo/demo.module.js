(function() {
    'use strict';

    angular
        .module('lonceldemo', [
            'ngMaterial',
            'ui.router'
        ]);
})();