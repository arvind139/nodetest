(function() {
    'use strict';

    angular
        .module('lonceldemo')
        .config(routeConfig);

    /* @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
        .state('demo', {
            url:"/listnodejscommits",
            templateUrl: 'app/lonceldemo/demo.tmpl.html',
            controller: 'DemoController',
            controllerAs: 'vm'
        });
        
    }
})();