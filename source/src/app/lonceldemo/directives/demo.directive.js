'use strict';

angular.module('lonceldemo')
.directive('itemlist', function() {
    return {
        restrict: 'E',
        scope:{
            sha: '='
        },
        link: function($scope, element, attrs) {
            var sha = $scope.sha;
            var lastCharacter = sha.substr(sha.length - 1);
             if(!isNaN(lastCharacter)){
                    $scope.displayRed = "displayRed";
            }
        },
        templateUrl:'app/lonceldemo/directives/commit-detail.html'
      };
    });
