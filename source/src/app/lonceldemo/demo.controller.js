
(function () {
    'use strict';

    angular
        .module('lonceldemo')
        .controller('DemoController', DemoController);

    function DemoController(api) {
        var vm = this;
        vm.error = false;
        api.gitRepo.repo.get(function(data) {
            if(data.error) vm.error = true;
            else vm.commitDetails = JSON.parse(data.commitsInfo);
            
        });
    }
})();