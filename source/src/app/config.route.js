(function() {
    'use strict';

    angular
        .module('app')
        .config(routeConfig);

    /* @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        //setup app routes
        $urlRouterProvider.when('', '/listnodejscommits');
        $urlRouterProvider.when('/', '/listnodejscommits');
    }
})();