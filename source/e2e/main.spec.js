'use strict';

describe('The main view', function () {
  beforeEach(function () {
    browser.get('http://localhost:3000/#/listnodejscommits');
  });

  it('expect the sha ending with number is diplayed in red', function () {
    browser.driver.sleep(6000).then(function () {
      element.all(by.className('displayRed')).each(function (elem) {
        elem.getText().then(function (sha) {
          var lastCharacter = sha.substr(sha.length - 1);
          expect(isNaN(lastCharacter)).toBeFalsy();
        });
      });
    });
  });
});
