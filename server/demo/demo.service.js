
var request = require('requestretry');
var utils = require('../utils');
var Config = require('../config');

module.exports = {
    getCommitsForNodeJSRepo :function(callback){
        request({
            url: utils.translate(Config.GITHUB_COMMITS_ENDPOINT,{"owner":Config.NODEJS_REPO_OWNER,"repo":Config.NODEJS_REPO}),
            method:"GET",
            headers:{
                'User-Agent': 'loncel_demo'
            },
            
            maxAttempts: 5,   // (default) try 5 times 
            retryDelay: 5000,  // (default) wait for 5s before trying again 
            retryStrategy: request.RetryStrategies.HTTPOrNetworkError // (default) retry on 5xx or network errors 
        }, function (err, response, body) {
            // this callback will only be called when the request succeeded or after maxAttempts or on error
            if(err)
                callback(err,null)
            else if (response) {
                console.log('The number of request attempts: ' + response.attempts); //for logging purpose
                callback(null,body);
            }
        });
    }
};