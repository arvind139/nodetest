
var demoService = require('./demo.service');

module.exports = {
    getNodeRepoCommits:function(req,res,next){
        demoService.getCommitsForNodeJSRepo(function(err,commitData){
            if(err)
                res.status(500).send({"error":err});
            else
                res.status(200).json({commitsInfo:commitData});
        });
    }
}