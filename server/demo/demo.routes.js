/**
 * Created by pankaj 16/07/2016
 */
var _ = require('underscore')
    , path = require('path')
    , DemoCtrl = require('./demo.controller')

var alertRoutes = [{
    path: '/fetchcommitdetails',
    httpMethod: 'GET',
    middleware: [DemoCtrl.getNodeRepoCommits]
}];

module.exports = function (app) {

    _.each(alertRoutes, function (route) {
        var args = _.flatten([route.path, route.middleware]);

        switch (route.httpMethod.toUpperCase()) {
            case 'GET':
                app.get.apply(app, args);
                break;
            case 'POST':
                app.post.apply(app, args);
                break;
            case 'PUT':
                app.put.apply(app, args);
                break;
            case 'DELETE':
                app.delete.apply(app, args);
                break;
            default:
                throw new Error('Invalid HTTP method specified for route ' + route.path);
                break;
        }
    });
};