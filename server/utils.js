
module.exports = {
    translate: function (o, a) {
        var j = JSON.stringify(o);
        for (var k in a) {
            j = j.split('${' + k + '}').join(a[k]);
        }
        return JSON.parse(j);
    }
};

