## Task Submission documentation

The folder named "server" is an express app that exposes a route to fetch commit details for node
repo.

Folder "source" is an angular app using angular material, that essentially consumes above route
and displays the list of commits. The commits whose sha are ending with number are colored red.

screenshot.png shows the app appearance on chrome browser.


## Install dependencies
1. Run npm install from root folder
2. Run npm install from source folder
3. Run bower install from source folder

## To run application
1. From project root run command - node server 
2. Concurrently in another terminal tab navigate to source folder run command - gulp serve
3. To run test run command  - gulp protractor:serve




## NodeJS Programming Task

NodeJS is Loncel's main programming language in the cloud space. It is quite important
that you feel comfortable with it. Hence this test.

*Note: This task should take no longer than 1-2 hours at the most.*


### Prerequisites

- Please note that this will require some basic [JavaScript](http://www.codecademy.com/tracks/javascript) and [ExpressJS](http://expressjs.com/) knowledge.

- You will need to have [NodeJS](http://www.nodejs.org/) installed to complete this task.

## Task

1. Fork this repository (if you don't know how to do that, Google is your friend)
2. Create a *source* folder to contain your code.
3. In the *source* directory, please create an ExpressJS app that accomplishes the following:
	- Connect to the [Github API](http://developer.github.com/)
	- Find the [nodejs](https://github.com/nodejs/node) repository
	- Find the most recent commits (choose at least 25 or more of the commits)
	- Create a route that displays the recent commits ordered by author.
	- If the commit hash ends in a number, color that row to red (#DC143C).

### Tests

Create the following unit test with the testing framework of your choice:

  1.  Verify that rows ending in a number are colored red.  

## Once Complete
1. Commit and Push your code to your new repository
2. Send us a pull request, we will review your code and get back to you