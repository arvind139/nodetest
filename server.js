var express =           require('express')
    , http =            require('http')
    , path =            require('path')
    , morgan =          require('morgan')
    , bodyParser =      require('body-parser')
    , methodOverride =  require('method-override')
    , cookieParser =    require('cookie-parser')
    , cookieSession =   require('cookie-session')
    , session =         require('express-session')
    , cors = require('cors');
  

var app  = module.exports =express();
//dev
app.set('views', __dirname + '/source/.tmp/serve');
//enable after build for prod
// app.set('views', __dirname + '/source/dist');
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride());
//dev
app.use(express.static(path.join(__dirname, '.tmp/serve')));
//enable after build for prod
// app.use(express.static(path.join(__dirname, '/source/dist')));
app.use(cookieParser());
app.use(cors());

var env = process.env.NODE_ENV || 'development';

require('./server/demo/demo.routes.js')(app);


app.set('port', process.env.PORT || 8000);
http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});

